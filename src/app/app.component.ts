import { Component } from '@angular/core';
import {Card, CardDeck } from 'src/lib/CardDeck';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
       cardDeck: CardDeck;
       cardsUser
  constructor() {
    this.cardDeck = new CardDeck();
    this.cardsUser = this.cardDeck.getCards(2);
    console.log(this.cardDeck);
  }
  addCardToUser() {
         this.cardsUser.push(this.cardDeck.getCard());
  }
  reset(){
    this.cardDeck = new CardDeck();
    this.cardsUser = this.cardDeck.getCards(2);
  }

  printScore() {
    let score: number = 0;
    for (let i = 0; i < this.cardsUser.length; i++) {
      const card = new Card(this.cardsUser[i].rank, this.cardsUser[i].suit);
      score += card.getScore();
    }
    return score;
  }
  switchOffButton() {
    return this.printScore() > 21 || this.printScore() === 21
  };

  getResult(){
         if(this.printScore() === 21){
           return 'You won';
         }
         if(this.printScore() > 21){
           return 'You lose';
         } else {
           return null;
         }
  }
}


