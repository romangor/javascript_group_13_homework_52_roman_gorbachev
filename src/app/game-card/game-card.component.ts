import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.css']
})
export class GameCardComponent {
  @Input() rank: string = '';
  @Input() suit: string = '';
           symbol: string = '';
  constructor() {
  }

  getClassname() {
    return `card rank-${this.rank.toLowerCase()} ${this.suit}`;
  }

  getSuit() {
    if (this.suit === 'diams') {
      return '♦';
    }
    if (this.suit === 'spades'){
      return  '♠';
    }
    if (this.suit === 'hearts'){
      return '♥';
    }
    if (this.suit === 'clubs'){
      return '♣';
    } else{
      return null;
    }
  }
}
