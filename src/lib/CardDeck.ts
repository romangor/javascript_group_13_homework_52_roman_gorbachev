
const RANKS = ['2','3','4','5','6','7','8','9','10','J','Q','K','A'];
const SUITS = ['diams','spades','hearts','clubs'];

export class Card {
  constructor(public rank:string,
              public suit:string) {
  }
  getScore(){
    if (this.rank === 'J' || this.rank === 'K' ||this.rank === 'Q'){
      return 10;
    } if (this.rank === 'A'){
      return 11;
    } else{
      return parseInt(this.rank);
    }
  }
}

export class CardDeck {
  cards: Card[] = [];
  constructor() {
    for (let i = 0; i < RANKS.length; i++){
      for (let j = 0; j < SUITS.length; j++){
        this.cards.push(new Card (RANKS[i], SUITS[j]));
      }
    }
  }

  getCard() {
    const randomNumber = Math.floor(Math.random() * (this.cards.length - 1 + 1)) + 1;
    return this.cards.splice(randomNumber, 1)[0];
      };

  getCards(howMany: number) {
    let cardsItems = [];
    for (let i = 0; i < howMany; i++) {
      cardsItems.push(this.getCard());
    }
    return cardsItems;
  }
}





